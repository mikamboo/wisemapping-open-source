<map version="0.9.0">
    <node BACKGROUND_COLOR="#0a0a08" COLOR="#dfcfe6" ID="ID_1" TEXT="Welcome To WiseMapping">
        <font/>
        <node BACKGROUND_COLOR="#250be3" COLOR="#ffffff" ID="ID_11" POSITION="right" TEXT="Try it Now!">
            <font/>
            <edge COLOR="#080559"/>
            <node COLOR="#001be6" ID="ID_12" POSITION="right" TEXT="Double Click">
                <font ITALIC="true"/>
            </node>
            <node COLOR="#001be6" ID="ID_13" POSITION="right" TEXT=" INS to insert">
                <font ITALIC="true"/>
            </node>
            <node COLOR="#001be6" ID="ID_14" POSITION="right" TEXT="Drag map to move">
                <font ITALIC="true"/>
            </node>
        </node>
        <node BACKGROUND_COLOR="#d9b518" COLOR="#104f11" ID="ID_2" POSITION="left" TEXT="Productivity">
            <icon BUILTIN="bar"/>
            <font/>
            <node ID="ID_3" POSITION="left" TEXT="Share your ideas">
                <icon BUILTIN="idea"/>
            </node>
            <node ID="ID_4" POSITION="left" TEXT="Brainstorming"/>
            <node ID="ID_5" POSITION="left" TEXT="Visual "/>
            <node ID="ID_27" POSITION="left" STYLE="image"/>
        </node>
        <node BACKGROUND_COLOR="#edabff" COLOR="#602378" ID="ID_6" POSITION="right" TEXT="Mind Mapping">
            <font/>
            <node ID="ID_7" POSITION="right" TEXT="Share with Collegues"/>
            <node ID="ID_8" POSITION="right" TEXT="Online"/>
            <node ID="ID_9" POSITION="right" TEXT="Anyplace, Anytime"/>
            <node ID="ID_10" POSITION="right" TEXT="Free!!!"/>
        </node>
        <node BACKGROUND_COLOR="#add1f7" COLOR="#0c1d6b" ID="ID_22" POSITION="left" TEXT="Web 2.0 Tool">
            <font/>
            <node ID="ID_23" POSITION="left" TEXT="Collaborate"/>
            <node ID="ID_24" POSITION="left" TEXT="No plugin required">
                <icon BUILTIN="disconnect"/>
            </node>
            <node ID="ID_25" POSITION="left" TEXT="Share"/>
            <node ID="ID_26" POSITION="left" TEXT="Easy to use"/>
        </node>
        <node ID="ID_15" POSITION="right" TEXT="Features">
            <node ID="ID_16" LINK="http://www.digg.com" POSITION="right" TEXT="Links to Sites">
                <font SIZE="10"/>
            </node>
            <node ID="ID_17" POSITION="right" TEXT="Fonts"/>
            <node ID="ID_18" POSITION="right" TEXT="Topic Color"/>
            <node ID="ID_19" POSITION="right" TEXT="Topic Shapes"/>
            <node ID="ID_20" POSITION="right" TEXT="Icons">
                <icon BUILTIN="rainbow"/>
            </node>
            <node ID="ID_21" POSITION="right" TEXT="History Changes">
                <icon BUILTIN="turn_left"/>
            </node>
        </node>
    </node>
</map>