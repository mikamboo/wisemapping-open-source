<map version="0.9.0">
    <node ID="ID_null" TEXT="corona">
        <node ID="ID_null" POSITION="left" TEXT="Modelo in world">
            <node ID="ID_null" POSITION="left" TEXT="International market protected Modelo from unstable peso"/>
            <node ID="ID_null" POSITION="left" TEXT="Fifth largest distributor in world">
                <node ID="ID_null" POSITION="left" TEXT="Can they sustain that trend"/>
                <node ID="ID_null" POSITION="left" TEXT="in 12 years"/>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="One of top 10 breweries in world"/>
        </node>
        <node ID="ID_null" POSITION="left" TEXT="Carloz Fernandez CEO">
            <node ID="ID_null" POSITION="left" TEXT="CEO Since 1997">
                <node ID="ID_null" POSITION="left" TEXT="29 years old">
                    <node ID="ID_null" POSITION="left" TEXT="working there since 13"/>
                </node>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="vision: top five brewers">
                <node ID="ID_null" POSITION="left" TEXT="International Business model">
                    <node ID="ID_null" POSITION="left" TEXT="experienced local distributors"/>
                    <node ID="ID_null" POSITION="left" TEXT="Growing international demand"/>
                    <node ID="ID_null" POSITION="left" TEXT="Capitalize on NAFTA"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="top 10 beer producers in world">
                    <node ID="ID_null" POSITION="left" TEXT="7.8 % sales growth compounded over ten years"/>
                    <node ID="ID_null" POSITION="left" TEXT="2005">
                        <node ID="ID_null" POSITION="left" TEXT="12.3 % exports"/>
                        <node ID="ID_null" POSITION="left" TEXT="4% increase domestically"/>
                        <node ID="ID_null" POSITION="left" TEXT="export sales 30%"/>
                    </node>
                    <node ID="ID_null" POSITION="left" TEXT="Corona Extra">
                        <node ID="ID_null" POSITION="left" TEXT="worlds fourth best selling beer"/>
                        <node ID="ID_null" POSITION="left" TEXT="56% shar of domestic market"/>
                        <node ID="ID_null" POSITION="left" TEXT="Since 1997 #1 import in US">
                            <node ID="ID_null" POSITION="left" TEXT="outsold competitor by 50%"/>
                        </node>
                    </node>
                </node>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="Expanding production ">
                <node ID="ID_null" POSITION="left" TEXT="renovate facility in Zacatecas"/>
                <node ID="ID_null" POSITION="left" TEXT="300 million investment"/>
            </node>
        </node>
        <node ID="ID_null" POSITION="left" TEXT="US Beer Market">
            <node ID="ID_null" POSITION="left" TEXT="2nd largest nest to China"/>
            <node ID="ID_null" POSITION="left" TEXT="Consumption six times higher per cap"/>
            <node ID="ID_null" POSITION="left" TEXT="Groth expectations reduced"/>
            <node ID="ID_null" POSITION="left" TEXT="80% of market">
                <node ID="ID_null" POSITION="left" TEXT="AB">
                    <node ID="ID_null" POSITION="left" TEXT="75% of industry profits"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="adolf coors"/>
                <node ID="ID_null" POSITION="left" TEXT="Miller"/>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="dense network of regional craft brewing"/>
            <node ID="ID_null" POSITION="left" TEXT="volume main driver"/>
        </node>
        <node ID="ID_null" POSITION="left" TEXT="Modelo in Mexico">
            <node ID="ID_null" POSITION="left" TEXT="History to 1970">
                <node ID="ID_null" POSITION="left" TEXT="formed in 1922">
                    <node ID="ID_null" POSITION="left" TEXT="Pablo Diez Fernandez, Braulio Irare, Marin Oyamburr"/>
                    <node ID="ID_null" POSITION="left" TEXT="Iriarte died in 1932"/>
                    <node ID="ID_null" POSITION="left" TEXT="Diez sole owner 1936"/>
                    <node ID="ID_null" POSITION="left" TEXT="Fernandez Family Sole owner since 1936"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="focus on Mexico City"/>
                <node ID="ID_null" POSITION="left" TEXT="Modelo 1st Brand"/>
                <node ID="ID_null" POSITION="left" TEXT="Corona 2nd Brand">
                    <node ID="ID_null" POSITION="left" TEXT="Clear Glass Customers preference"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="1940s period of strong growth ">
                    <node ID="ID_null" POSITION="left" TEXT="concentrate domesti&#172;cally "/>
                    <node ID="ID_null" POSITION="left" TEXT="improve distribution methods and produc&#172;tion facilities ">
                        <node ID="ID_null" POSITION="left" TEXT="distribution: direct with profit sharing"/>
                    </node>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="bought the brands and assets of the Toluca y Mexico Brewery">
                    <node ID="ID_null" POSITION="left" TEXT="1935"/>
                    <node ID="ID_null" POSITION="left" TEXT="country's oldest brand of beer"/>
                </node>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="1971, Antonino Fernandez was appointed CEO">
                <node ID="ID_null" POSITION="left" TEXT="Mexican Stock exchange in 1994"/>
                <node ID="ID_null" POSITION="left" TEXT="Anheuser-Busch 17.7 % of the equity">
                    <node ID="ID_null" POSITION="left" TEXT="The 50.2 % represented 43.9%  voting"/>
                </node>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="Largest Beer producer and distrubutor in Mexico">
                <node ID="ID_null" POSITION="left" TEXT="corona 56% share"/>
            </node>
        </node>
        <node ID="ID_null" POSITION="right" TEXT="Modelo in US">
            <node ID="ID_null" POSITION="left" TEXT="History">
                <node ID="ID_null" POSITION="left" TEXT="1979"/>
                <node ID="ID_null" POSITION="left" TEXT="Amalgamated Distillery Products Inc. (">
                    <node ID="ID_null" POSITION="left" TEXT="later renamed Barton Beers Ltd."/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="gained popularity in southern states"/>
                <node ID="ID_null" POSITION="left" TEXT="rapid growth 1980s">
                    <node ID="ID_null" POSITION="left" TEXT="second most popular imported beer"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="1991">
                    <node ID="ID_null" POSITION="left" TEXT="doubling of federal excise tax on beer">
                        <node ID="ID_null" POSITION="left" TEXT="sales decrease of 15 percent"/>
                        <node ID="ID_null" POSITION="left" TEXT="distributor absorb the tax 92"/>
                    </node>
                    <node ID="ID_null" POSITION="left" TEXT="distributors took the loss"/>
                </node>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="2007 5 beers to us">
                <node ID="ID_null" POSITION="left" TEXT="3 of top 8 beers in US"/>
                <node ID="ID_null" POSITION="left" TEXT="Heineken">
                    <node ID="ID_null" POSITION="left" TEXT="Main Import Comptitor"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="131 million cases"/>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="Marketing">
                <node ID="ID_null" POSITION="left" TEXT="surfing mythology"/>
                <node ID="ID_null" POSITION="left" TEXT="not selling premium quality"/>
                <node ID="ID_null" POSITION="left" TEXT="not testosterone driven"/>
                <node ID="ID_null" POSITION="left" TEXT="found new following"/>
                <node ID="ID_null" POSITION="left" TEXT="beer for non beer drinkers"/>
                <node ID="ID_null" POSITION="left" TEXT="dependable second choise"/>
                <node ID="ID_null" POSITION="left" TEXT="Fun in the sun">
                    <node ID="ID_null" POSITION="left" TEXT="Barton Beer's idea"/>
                    <node ID="ID_null" POSITION="left" TEXT="escape"/>
                    <node ID="ID_null" POSITION="left" TEXT="relaxation"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="1996ad budget">
                    <node ID="ID_null" POSITION="left" TEXT="Corona 5.1 mil"/>
                    <node ID="ID_null" POSITION="left" TEXT="Heiniken 15 mil"/>
                    <node ID="ID_null" POSITION="left" TEXT="an bsch 192 mil"/>
                </node>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="Us dist contracts">
                <node ID="ID_null" POSITION="left" TEXT="importer/distributors">
                    <node ID="ID_null" POSITION="left" TEXT="Local Companies"/>
                    <node ID="ID_null" POSITION="left" TEXT="Autonomous"/>
                    <node ID="ID_null" POSITION="left" TEXT="competitive relationship"/>
                    <node ID="ID_null" POSITION="left" TEXT="transportation"/>
                    <node ID="ID_null" POSITION="left" TEXT="insurance"/>
                    <node ID="ID_null" POSITION="left" TEXT="pricing"/>
                    <node ID="ID_null" POSITION="left" TEXT="customs"/>
                    <node ID="ID_null" POSITION="left" TEXT="advertixing"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="procermex inc">
                    <node ID="ID_null" POSITION="left" TEXT="Modelo us subsidiary"/>
                    <node ID="ID_null" POSITION="left" TEXT="Support"/>
                    <node ID="ID_null" POSITION="left" TEXT="Supervise"/>
                    <node ID="ID_null" POSITION="left" TEXT="Coordinate"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="Modelo had final say on brand image"/>
                <node ID="ID_null" POSITION="left" TEXT="production in Mexico"/>
                <node ID="ID_null" POSITION="left" TEXT="Chicago based Barton Beers 1st">
                    <node ID="ID_null" POSITION="left" TEXT="largest importer in 25 western states"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="Gambrinus">
                    <node ID="ID_null" POSITION="left" TEXT="1986"/>
                    <node ID="ID_null" POSITION="left" TEXT="eastern dist"/>
                </node>
            </node>
        </node>
        <node ID="ID_null" POSITION="right" TEXT="The Beer market">
            <node ID="ID_null" POSITION="left" TEXT="traditionally a clustered market"/>
            <node ID="ID_null" POSITION="left" TEXT="many local breweries"/>
            <node ID="ID_null" POSITION="left" TEXT="no means of transport"/>
            <node ID="ID_null" POSITION="left" TEXT="colsolition happened in 1800s"/>
            <node ID="ID_null" POSITION="left" TEXT="different countries had different tastes"/>
            <node ID="ID_null" POSITION="left" TEXT="90s national leaders expanded abroad"/>
            <node ID="ID_null" POSITION="left" TEXT="startup costs high">
                <node ID="ID_null" POSITION="left" TEXT="industry supported conectration"/>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="Interbrew">
                <node ID="ID_null" POSITION="left" TEXT="Belgian"/>
                <node ID="ID_null" POSITION="left" TEXT="aquired breweries in 20 countries"/>
                <node ID="ID_null" POSITION="left" TEXT="sales in 110 countries"/>
                <node ID="ID_null" POSITION="left" TEXT="local managers controlling brands"/>
                <node ID="ID_null" POSITION="left" TEXT="flagship brand: Stella Artois"/>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="2004 merger">
                <node ID="ID_null" POSITION="left" TEXT="#1 Interbrew"/>
                <node ID="ID_null" POSITION="left" TEXT="#5 Am Bev - Brazil"/>
                <node ID="ID_null" POSITION="left" TEXT="largest merge">
                    <node ID="ID_null" POSITION="left" TEXT="worth 12.8 billion"/>
                </node>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="2007">
                <node ID="ID_null" POSITION="left" TEXT="inbev"/>
                <node ID="ID_null" POSITION="left" TEXT="SAP Miller"/>
                <node ID="ID_null" POSITION="left" TEXT="Heineken">
                    <node ID="ID_null" POSITION="left" TEXT="produces beer domestically">
                        <node ID="ID_null" POSITION="left" TEXT="parent of local distributors">
                            <node ID="ID_null" POSITION="left" TEXT="marketing"/>
                            <node ID="ID_null" POSITION="left" TEXT="importing">
                                <node ID="ID_null" POSITION="left" TEXT="import taxes passed on to consumer"/>
                            </node>
                            <node ID="ID_null" POSITION="left" TEXT="distribution"/>
                        </node>
                    </node>
                    <node ID="ID_null" POSITION="left" TEXT="marketing">
                        <node ID="ID_null" POSITION="left" TEXT="premium beer"/>
                        <node ID="ID_null" POSITION="left" TEXT="premium brand"/>
                        <node ID="ID_null" POSITION="left" TEXT="no mythology"/>
                        <node ID="ID_null" POSITION="left" TEXT="superior taste"/>
                        <node ID="ID_null" POSITION="left" TEXT="2006 aggressive marketing campaign">
                            <node ID="ID_null" POSITION="left" TEXT="Heineken Premium Light"/>
                        </node>
                    </node>
                    <node ID="ID_null" POSITION="left" TEXT="reputation of top selling beer in world"/>
                    <node ID="ID_null" POSITION="left" TEXT="Dutch"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="Anh Bush">
                    <node ID="ID_null" POSITION="left" TEXT="produces in foreign markets"/>
                </node>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="Beer Marketing">
                <node ID="ID_null" POSITION="left" TEXT="People drink marketing"/>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="Future">
                <node ID="ID_null" POSITION="left" TEXT="domestic and foreign threats"/>
                <node ID="ID_null" POSITION="left" TEXT="other merger talks"/>
                <node ID="ID_null" POSITION="left" TEXT="Inbev in talks with Anh Bush">
                    <node ID="ID_null" POSITION="left" TEXT="Two biggest companies will create huge company"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="Sales were decreasing due to competitive media budgets"/>
            </node>
        </node>
        <node ID="ID_null" POSITION="right" TEXT="Mexico Industry">
            <node ID="ID_null" POSITION="left" TEXT="has most trade agreements in world"/>
            <node ID="ID_null" POSITION="left" TEXT="one of the largest domestic beer markets"/>
            <node ID="ID_null" POSITION="left" TEXT="imported beer only 1% sales">
                <node ID="ID_null" POSITION="left" TEXT="half were anh bcsh dist by modelo"/>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="modelo">
                <node ID="ID_null" POSITION="left" TEXT="NAFTA S.A. An Bucsh"/>
                <node ID="ID_null" POSITION="left" TEXT="62.8% of market"/>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="FEMSA">
                <node ID="ID_null" POSITION="left" TEXT="domestic market">
                    <node ID="ID_null" POSITION="left" TEXT="37% of domestic market"/>
                    <node ID="ID_null" POSITION="left" TEXT="production and distribution in Mexico: peso not a threat"/>
                    <node ID="ID_null" POSITION="left" TEXT="Owns Oxxo C">
                        <node ID="ID_null" POSITION="left" TEXT="CA largest chain of conv stores"/>
                    </node>
                    <node ID="ID_null" POSITION="left" TEXT="leads domestic premium beer market"/>
                    <node ID="ID_null" POSITION="left" TEXT="997 to 2004 taking domestic market share"/>
                    <node ID="ID_null" POSITION="left" TEXT="NAFTA SACoca cola">
                        <node ID="ID_null" POSITION="left" TEXT="Exclusive distributor"/>
                    </node>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="foriegn market">
                    <node ID="ID_null" POSITION="left" TEXT="Partnership Heiniken">
                        <node ID="ID_null" POSITION="left" TEXT="Distribution in US"/>
                    </node>
                    <node ID="ID_null" POSITION="left" TEXT="90s entry to us market failed"/>
                    <node ID="ID_null" POSITION="left" TEXT="Recently partnered with Heiniken for US market">
                        <node ID="ID_null" POSITION="left" TEXT="2005 18.7% growth"/>
                    </node>
                </node>
            </node>
        </node>
    </node>
</map>