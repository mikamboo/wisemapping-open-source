<map version="0.9.0">
    <node BACKGROUND_COLOR="#32e36a" ID="ID_1" TEXT="PPM Plan">
        <node ID="ID_4" POSITION="right" TEXT="Business Development ">
            <font BOLD="true"/>
        </node>
        <node ID="ID_18" LINK="https://docs.google.com/a/freeform.ca/drawings/d/1mrtkVAN3_XefJJCgfxw4Va6xk9TVDBKXDt_uzyIF4Us/edit" POSITION="right" TEXT="Backlog Management">
            <font BOLD="true"/>
        </node>
        <node ID="ID_10" POSITION="left" TEXT="Freeform IT">
            <font BOLD="true"/>
        </node>
        <node ID="ID_204" POSITION="left" TEXT="Client Project Management">
            <font BOLD="true"/>
        </node>
        <node ID="ID_206" POSITION="left" TEXT="Governance &amp; Executive">
            <font BOLD="true"/>
        </node>
        <node ID="ID_5" POSITION="right" TEXT="Finance">
            <font BOLD="true"/>
        </node>
        <node ID="ID_3" POSITION="right" TEXT="Administration">
            <font BOLD="true"/>
        </node>
        <node ID="ID_154" POSITION="right" TEXT="Human Resources">
            <font BOLD="true"/>
            <richcontent TYPE="NOTE">
                <html>
                    <head/>
                    <body>
                        <p/>
                    </body>
                </html>
            </richcontent>
        </node>
        <node ID="ID_16" POSITION="left" TEXT="Freeform Hosting">
            <font BOLD="true"/>
        </node>
        <node ID="ID_247" POSITION="right" TEXT="Community Outreach">
            <font BOLD="true"/>
        </node>
        <node ID="ID_261" POSITION="right" TEXT="R&amp;D">
            <font BOLD="true"/>
            <node ID="ID_263" POSITION="right" TEXT="Goals"/>
            <node ID="ID_264" POSITION="right" TEXT="Formulize"/>
        </node>
        <node ID="ID_268" POSITION="left" TEXT="Probono">
            <node ID="ID_269" POSITION="left"/>
        </node>
    </node>
</map>