<map version="0.9.0">
    <node ID="ID_null" TEXT="I Care">
        <edge COLOR="#121110"/>
        <node ID="ID_null" POSITION="right" TEXT="veiligheid">
            <node ID="ID_null" POSITION="left" TEXT="verkeer">
                <richcontent TYPE="NOTE">
                    <html>
                        <head/>
                        <body>
                            <p/>
                        </body>
                    </html>
                </richcontent>
                <node ID="ID_null" POSITION="left" TEXT="filevorming">
                    <node ID="ID_null" POSITION="left" TEXT="preventie">
                        <richcontent TYPE="NOTE">
                            <html>
                                <head/>
                                <body>
                                    <p/>
                                </body>
                            </html>
                        </richcontent>
                    </node>
                    <node ID="ID_null" POSITION="left" TEXT="Omleiding zoeken"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="Openbaar vervoer"/>
                <node ID="ID_null" POSITION="left" TEXT="Openbare fietsen">
                    <node ID="ID_null" POSITION="left" TEXT="Trackingsysteem, # fietsen op 1 plaats"/>
                    <node ID="ID_null" POSITION="left" TEXT="Fiets bestellen in grote steden"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="ongevallen">
                    <node ID="ID_null" POSITION="left" TEXT="preventie">
                        <node ID="ID_null" POSITION="left" TEXT="roekeloos rijgedrag opsporen">
                            <node ID="ID_null" POSITION="left" TEXT="via sensoren aan boord versnelling, manouvres, snelheid,... registreren"/>
                        </node>
                        <node ID="ID_null" POSITION="left" TEXT="bestraffen van chauffeurs?"/>
                    </node>
                    <node ID="ID_null" POSITION="left" TEXT="oorzaak">
                        <node ID="ID_null" POSITION="left" TEXT="nagaan van de oorzaak"/>
                        <node ID="ID_null" POSITION="left" TEXT="Automatisch opbellen van hulpdiensten"/>
                        <node ID="ID_null" POSITION="left" TEXT="opsporen van vluchtmisdrijf"/>
                    </node>
                    <node ID="ID_null" POSITION="left" TEXT="info over de staat van het wegdek">
                        <node ID="ID_null" POSITION="left" TEXT="wegenwerken"/>
                        <node ID="ID_null" POSITION="left" TEXT="waarschuwingen"/>
                        <node ID="ID_null" POSITION="left" TEXT="aanpassing van max. snelheid"/>
                    </node>
                </node>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="criminaliteit">
                <richcontent TYPE="NOTE">
                    <html>
                        <head/>
                        <body>
                            <p/>
                        </body>
                    </html>
                </richcontent>
                <node ID="ID_null" POSITION="left" TEXT="drugs">
                    <node ID="ID_null" POSITION="left" TEXT="opsporen">
                        <node ID="ID_null" POSITION="left" TEXT="individuele tests"/>
                        <node ID="ID_null" POSITION="left" TEXT="testen per woonregio (via afvoerstelsels)"/>
                    </node>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="wapenhandel">
                    <node ID="ID_null" POSITION="left" TEXT="opsporen"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="diefstal">
                    <node ID="ID_null" POSITION="left" TEXT="tracking via gps"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="Aantal ongevallen/criminele feiten registreren">
                    <node ID="ID_null" POSITION="left" TEXT="Mapping van probleemsituaties"/>
                    <node ID="ID_null" POSITION="left" TEXT="Meer controle"/>
                    <node ID="ID_null" POSITION="left" TEXT="Terugkoppeling naar politie/verkeersdienst"/>
                </node>
            </node>
        </node>
        <node ID="ID_null" POSITION="left" STYLE="bubble" TEXT="De markt">
            <richcontent TYPE="NOTE">
                <html>
                    <head/>
                    <body>
                        <p/>
                    </body>
                </html>
            </richcontent>
            <node ID="ID_null" POSITION="left" STYLE="rectagle" TEXT="kleding">
                <node ID="ID_null" POSITION="left" TEXT="informatiebronnen per merk">
                    <node ID="ID_null" POSITION="left" TEXT="slimme kledij"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="kwaliteitsinfo bij aankoop">
                    <node ID="ID_null" POSITION="left" TEXT="info uit cloud"/>
                    <node ID="ID_null" POSITION="left" TEXT="reviews"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="monitoring">
                    <richcontent TYPE="NOTE">
                        <html>
                            <head/>
                            <body>
                                <p/>
                            </body>
                        </html>
                    </richcontent>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="stijlen">
                    <node ID="ID_null" POSITION="left" TEXT="begeleiding bij het winkelen naar gelijkaardige kleding"/>
                </node>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="voeding">
                <node ID="ID_null" POSITION="left" TEXT="zie bij handelingen --&gt;winkelen"/>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="electronica">
                <node ID="ID_null" POSITION="left" TEXT="eigendomsverificatie">
                    <node ID="ID_null" POSITION="left" TEXT="GPS tracking"/>
                    <node ID="ID_null" POSITION="left" TEXT="koop-verkoop"/>
                    <node ID="ID_null" POSITION="left" TEXT="afdanking (sluikstorten)"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="monitoring">
                    <node ID="ID_null" POSITION="left" TEXT="kwaliteit">
                        <node ID="ID_null" POSITION="left" TEXT="rechtstreekse link naar producent bij falen"/>
                        <node ID="ID_null" POSITION="left" TEXT="reparatieservice"/>
                    </node>
                    <node ID="ID_null" POSITION="left" TEXT="levensloop">
                        <node ID="ID_null" POSITION="left" TEXT="gebruikscycli"/>
                    </node>
                    <node ID="ID_null" POSITION="left" TEXT="productie verloop">
                        <node ID="ID_null" POSITION="left" TEXT="door wie"/>
                        <node ID="ID_null" POSITION="left" TEXT="wat/wanneer"/>
                        <node ID="ID_null" POSITION="left" TEXT="testfasen"/>
                    </node>
                </node>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="medicijnen">
                <node ID="ID_null" POSITION="left" TEXT="kwaliteit"/>
                <node ID="ID_null" POSITION="left" TEXT="werking"/>
                <node ID="ID_null" POSITION="left" TEXT="neveneffecten"/>
                <node ID="ID_null" POSITION="left" TEXT="alternatieven"/>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="inrichting (woning)">
                <node ID="ID_null" POSITION="left" TEXT="monitoring">
                    <node ID="ID_null" POSITION="left" TEXT="kwaliteit"/>
                    <node ID="ID_null" POSITION="left" TEXT="eigendomsverificatie"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="advies">
                    <node ID="ID_null" POSITION="left" TEXT="kleuren en stijlen"/>
                </node>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="energie">
                <node ID="ID_null" POSITION="left" TEXT="automatische lichten"/>
                <node ID="ID_null" POSITION="left" TEXT="kamer herkent gebruiker"/>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="diensten"/>
            <node ID="ID_null" POSITION="left" TEXT="hygi&#235;ne"/>
        </node>
        <node BACKGROUND_COLOR="#ffffff" ID="ID_null" POSITION="left" TEXT="handelingen">
            <node ID="ID_null" POSITION="left" TEXT="winkelen">
                <font BOLD="true"/>
                <node ID="ID_null" POSITION="left" TEXT="winkelhulp">
                    <node ID="ID_null" POSITION="left" TEXT="productinfo">
                        <node ID="ID_null" POSITION="left" TEXT="allergie">
                            <node ID="ID_null" POSITION="left" TEXT="Gezondheidscontrole"/>
                            <node ID="ID_null" POSITION="left" TEXT="Sensor voor vers fruit en vlees"/>
                            <node ID="ID_null" POSITION="left" TEXT="Salmonella sensor"/>
                        </node>
                        <node ID="ID_null" POSITION="left" TEXT="prijs"/>
                        <node ID="ID_null" POSITION="left" TEXT="kwaliteit">
                            <node ID="ID_null" POSITION="left" TEXT="via cloud"/>
                            <node ID="ID_null" POSITION="left" TEXT="databases v. reviews"/>
                            <node ID="ID_null" POSITION="left" TEXT="bio"/>
                            <node ID="ID_null" POSITION="left" TEXT="voedingstoffen"/>
                        </node>
                        <node ID="ID_null" POSITION="left" TEXT="di&#235;et checker"/>
                        <node ID="ID_null" POSITION="left" TEXT="alternatieve producten"/>
                    </node>
                    <node ID="ID_null" POSITION="left" TEXT="digitale portemonnee"/>
                    <node ID="ID_null" POSITION="left" TEXT="bestellingen op afstand"/>
                    <node ID="ID_null" POSITION="left" TEXT="recepten generator">
                        <richcontent TYPE="NOTE">
                            <html>
                                <head/>
                                <body>
                                    <p/>
                                </body>
                            </html>
                        </richcontent>
                    </node>
                    <node ID="ID_null" POSITION="left" TEXT="boodschappenlijst">
                        <node ID="ID_null" POSITION="left" TEXT="dichtste bij bovenaan"/>
                        <node ID="ID_null" POSITION="left" TEXT="alternatieven indien uitverkocht"/>
                    </node>
                </node>
            </node>
        </node>
        <node ID="ID_null" POSITION="right" TEXT="milieu">
            <node ID="ID_null" POSITION="left" TEXT="bosbouw">
                <node ID="ID_null" POSITION="left" TEXT="ziektes bij bomen"/>
                <node ID="ID_null" POSITION="left" TEXT="parasieten"/>
                <node ID="ID_null" POSITION="left" TEXT="bodemvervuiling">
                    <node ID="ID_null" POSITION="left" TEXT="onderzoek"/>
                </node>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="mag ik dit door mijn gootsteen kappen"/>
            <node ID="ID_null" POSITION="left" TEXT="pollutie"/>
            <node ID="ID_null" POSITION="left" TEXT="afval">
                <node ID="ID_null" POSITION="left" TEXT="verwerking"/>
                <node ID="ID_null" POSITION="left" TEXT="riolen"/>
                <node ID="ID_null" POSITION="left" TEXT="cradle 2 cradle"/>
                <node ID="ID_null" POSITION="left"/>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="dioxines"/>
            <node ID="ID_null" POSITION="left" TEXT="eco systemen">
                <node ID="ID_null" POSITION="left" TEXT="diversiteit"/>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="waterreserves">
                <node ID="ID_null" POSITION="left" TEXT="reserves"/>
                <node ID="ID_null" POSITION="left" TEXT="vervuiling"/>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="alternatieve energie">
                <node ID="ID_null" POSITION="left" TEXT="wind"/>
                <node ID="ID_null" POSITION="left" TEXT="zon"/>
                <node ID="ID_null" POSITION="left" TEXT="kernenergie"/>
            </node>
        </node>
        <node ID="ID_null" POSITION="right" TEXT="industrie"/>
        <node ID="ID_null" POSITION="right" TEXT="cultuur">
            <node ID="ID_null" POSITION="left" TEXT="festival"/>
        </node>
        <node ID="ID_null" POSITION="right" TEXT="bouw">
            <richcontent TYPE="NOTE">
                <html>
                    <head/>
                    <body>
                        <p/>
                    </body>
                </html>
            </richcontent>
            <node ID="ID_null" POSITION="left" TEXT="opvolging zoals plannen zijn getekend">
                <node ID="ID_null" POSITION="left" TEXT="isolatie">
                    <node ID="ID_null" POSITION="left" TEXT="warmtemetingen"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="juistheid van materialen"/>
                <node ID="ID_null" POSITION="left" TEXT="nameten"/>
            </node>
        </node>
        <node ID="ID_null" POSITION="left" TEXT="aandoeningen en situaties">
            <node ID="ID_null" POSITION="left" TEXT="zwangerschap">
                <font BOLD="true"/>
                <node ID="ID_null" POSITION="left" TEXT="baby monitoren"/>
                <node ID="ID_null" POSITION="left" TEXT="info over voeding">
                    <node ID="ID_null" POSITION="left" TEXT="wat is gezond"/>
                    <node ID="ID_null" POSITION="left" TEXT="via cloud info over voeding">
                        <richcontent TYPE="NOTE">
                            <html>
                                <head/>
                                <body>
                                    <p/>
                                </body>
                            </html>
                        </richcontent>
                    </node>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="geboortetimer">
                    <richcontent TYPE="NOTE">
                        <html>
                            <head/>
                            <body>
                                <p/>
                            </body>
                        </html>
                    </richcontent>
                </node>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="dementen">
                <node ID="ID_null" POSITION="left" TEXT="wegloopdetectie"/>
                <node ID="ID_null" POSITION="left" TEXT="wassen"/>
                <node ID="ID_null" POSITION="left" TEXT="eten"/>
            </node>
        </node>
        <node ID="ID_null" POSITION="right" TEXT="sport">
            <node ID="ID_null" POSITION="left" TEXT="opvolging van de sporter">
                <node ID="ID_null" POSITION="left" TEXT="hartslag"/>
                <node ID="ID_null" POSITION="left" TEXT="bloeddruk"/>
                <node ID="ID_null" POSITION="left" TEXT="energieverbruik"/>
                <node ID="ID_null" POSITION="left" TEXT="cadans"/>
                <node ID="ID_null" POSITION="left" TEXT="gps"/>
            </node>
        </node>
        <node ID="ID_null" POSITION="left" TEXT="ziektes">
            <node ID="ID_null" POSITION="left" TEXT="verschillende ziektes">
                <richcontent TYPE="NOTE">
                    <html>
                        <head/>
                        <body>
                            <p/>
                        </body>
                    </html>
                </richcontent>
                <node ID="ID_null" POSITION="left" TEXT="feedback van de lichaamsconditie">
                    <node ID="ID_null" POSITION="left" TEXT="aan de pati&#235;nt">
                        <node ID="ID_null" POSITION="left" TEXT="waarschuwingen"/>
                    </node>
                    <node ID="ID_null" POSITION="left" TEXT="link">
                        <node ID="ID_null" POSITION="left" TEXT="naar ziekenhuis"/>
                        <node ID="ID_null" POSITION="left" TEXT="naar hulpdiensten"/>
                        <node ID="ID_null" POSITION="left" TEXT="naar behandelend arts"/>
                    </node>
                    <node ID="ID_null" POSITION="left" TEXT="aan dokter">
                        <node ID="ID_null" POSITION="left" TEXT="contacteert patient indien nodig"/>
                        <node ID="ID_null" POSITION="left" TEXT="volledig overzicht"/>
                        <node ID="ID_null" POSITION="left" TEXT="op afstand consulatie"/>
                    </node>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="MS">
                    <node ID="ID_null" POSITION="left" TEXT="bevorderen van communicatie tijdens de aftakeling">
                        <node ID="ID_null" POSITION="left" TEXT="naar de dokters toe"/>
                        <node ID="ID_null" POSITION="left" TEXT="naar familie en vrienden toe"/>
                    </node>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="mucoviscidose">
                    <node ID="ID_null" POSITION="left" TEXT="longcapaciteit meten"/>
                    <node ID="ID_null" POSITION="left" TEXT="alert voor donor"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="aids">
                    <node ID="ID_null" POSITION="left" TEXT="vergroten van de database aan info"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="diabetes">
                    <node ID="ID_null" POSITION="left" TEXT="suikerspiegel meten">
                        <node ID="ID_null" POSITION="left" TEXT="alert indien te laag"/>
                        <node ID="ID_null" POSITION="left" TEXT="automatische inspuiting"/>
                    </node>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="epilepsie">
                    <node ID="ID_null" POSITION="left" TEXT="aanval voorspellen??"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="astma">
                    <node ID="ID_null" POSITION="left" TEXT="detectie van luchtkwaliteit">
                        <node ID="ID_null" POSITION="left" TEXT="in kaart brengen van">
                            <node ID="ID_null" POSITION="left" TEXT="vervuiling"/>
                            <node ID="ID_null" POSITION="left" TEXT="pollen"/>
                            <node ID="ID_null" POSITION="left" TEXT="allergie veroorzakende deeltjes"/>
                        </node>
                    </node>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="anorexia">
                    <node ID="ID_null" POSITION="left" TEXT="nagaan of ze eten"/>
                    <node ID="ID_null" POSITION="left" TEXT="eten ze voldoende"/>
                </node>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="thuisverzorging">
                <node ID="ID_null" POSITION="left" TEXT="meting van de symptomen">
                    <node ID="ID_null" POSITION="left" TEXT="toevoeging van eigen waarneming"/>
                    <node ID="ID_null" POSITION="left" TEXT="objectieve metingen"/>
                </node>
                <node ID="ID_null" POSITION="left" TEXT="link naar dokter"/>
                <node ID="ID_null" POSITION="left" TEXT="link naar ziekenhuis"/>
            </node>
            <node ID="ID_null" POSITION="left" TEXT="stress">
                <node ID="ID_null" POSITION="left" TEXT="afreageren"/>
                <node ID="ID_null" POSITION="left" TEXT="monitoring">
                    <node ID="ID_null" POSITION="left" TEXT="in welke afdeling een probleem">
                        <node ID="ID_null" POSITION="left" TEXT="aan welke factoren ligt dat"/>
                        <node ID="ID_null" POSITION="left" TEXT="aanpak"/>
                    </node>
                </node>
            </node>
        </node>
    </node>
</map>